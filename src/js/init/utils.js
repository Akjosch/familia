/* eslint-disable no-unused-vars */
/**
 * Roll a dice.
 * 
 * Supported variants:
 * 
 * * a plain number; no rolling done, just returns that number
 * * a percentage chance as string ("15%", "99%"); returns 1 with the given percent chance, else 0
 * * a (number)d(size) notation ("3d6", "1d20", "7d123"); rolls that many-sided die and returns the sum
 *   if the number is omitted, 1 is assumed ("d8" is the same as "1d8")
 *   if the size is ommited, 6 is assumed ("5d" is the same as "5d6")
 * * a (number)d(size)kh(keep) notation, for rolling as above, but keeping only "keep" highest dice
 * * a (number)d(size)kl(keep) notation, for rolling as above, but keeping only "keep" lowest dice
 * * an additional +(value) or -(value) constant added at the end of the dice for all but
 *   the plain number and percentage notation
 * 
 * @param {string|number} die
 * @param {() => number} [rnd]
 */
function roll(die, rnd) {
	rnd = rnd || Math.random;
	if(typeof die === "string" && die.match(/^\d+$/)) {
			return Number(die);
	}
	if(typeof die === "number") {
			return die;
	}
	var trimmed = String(die).trim();
	var re = /^(\d+)%$/;
	var res = re.exec(trimmed);
	if(res) {
		var chance = Number(res[1]);
		return (Math.floor(rnd() * 100) < chance ? 1 : 0)
	}
	re = /^(\d*)[dD](\d*)((kh|kl)(\d+))?([+-]\d+)?$/
	res = re.exec(trimmed);
	if(!res) {
			return 0;
	}
	var num = Number(res[1] || "1");
	var size = Number(res[2] || "6");
	var nummod = res[4];
	var keep = Number(res[5] || "+Infinity");
	var add = Number(res[6] || "0");

	var rolls = [];
	for(var i = 0; i < num; ++ i) {
			rolls.push(Math.floor(rnd() * size) + 1);
	}
	rolls.sort();
	if(!!nummod && nummod.toLowerCase() === "kl" && keep <= rolls.length) {
			rolls.splice(keep);
	} else if(!!nummod && nummod.toLowerCase() === "kh") {
			rolls.splice(0, rolls.length - keep);
	}
	return add + rolls.reduce((a, b) => a + b, 0);
}
setup.roll = roll;

/**
 * @param {any[]} weights
 * @param {() => number} [rnd]
 */
function weightedRandom(weights, rnd) {
	if(!weights || !weights.length) {
		return undefined;
	}
	rnd = rnd || Math.random;
	var fallback = weights.last();
	var pick = rnd() * weights.reduce((sum, ch) => sum + ch.weight, 0);
	return (weights.find((ch) => (pick -= ch.weight) <= 0) || fallback);
}
setup.weightedRandom = weightedRandom;

/**
 * Generate until the filter returns true for the generated value
 * @param {() => any} generator
 * @param {(val: any) => boolean} [filter]
 * @param {number=} maxTries
 */
function genFiltered(generator, filter = ((ignore) => true), maxTries = 1000000) {
	var tries = ((Number.isFinite(maxTries) && maxTries > 0) ? maxTries : 1000000);
	do {
		var result = generator();
		-- tries;
	} while(!filter(result) && tries > 0);
	if(tries == 0) {
		return undefined;
	} else {
		return result;
	}
}
setup.genFiltered = genFiltered;

const InspectablePromise = (function() {
	var statusProp = Symbol("status");
	var progressProp = Symbol("progress");
	var valueProp = Symbol("value");
	var reasonProp = Symbol("reason");

	/**
	 * Inspectable Promise
	 */
	class InspectablePromise extends Promise {
		/**
		 * @param {(resolve: (value: any) => void, reject?: (reason: any) => void, progress?: (progress: any) => void) => void} executor
		 */
		constructor(executor) {
			var self;
			super((resolve, reject) => {
				executor(
					(value) => {
						if(self) {
							self[statusProp] = "resolved";
							self[valueProp] = value;
							resolve(value);
						} else {
							setTimeout(() => {
								self[statusProp] = "resolved";
								self[valueProp] = value;
								resolve(value);
							}, 0);
						}
					},
					(reason) => {
						if(self) {
							self[statusProp] = "rejected";
							self[reasonProp] = reason;
							reject(reason);
						} else {
							setTimeout(() => {
								self[statusProp] = "rejected";
								self[reasonProp] = reason;
								reject(reason);
							}, 0);
						}
					},
					(progress) => {
						if(self) {
							self[progressProp] = progress;
						} else {
							setTimeout(() => { self[progressProp] = progress; }, 0);
						}
					}
				);
			});
			self = this;
			this[statusProp] = "pending";
		}

		get status() { return this[statusProp]; }
		get pending() { return this[statusProp] === "pending"; }
		get resolved() { return this[statusProp] === "resolved"; }
		get rejected() { return this[statusProp] === "rejected"; }
		get value() { return this[valueProp]; }
		get reason() { return this[reasonProp]; }
		get progress() { return this[progressProp]; }
	}

	return InspectablePromise;
})();
setup.InspectablePromise = InspectablePromise;

/**
 * Return a Promise which will hopefully eventually return a filtered generator result
 * @param {() => any} generator
 * @param {(val: any) => boolean} [filter]
 * @param {number} maxtime - in ms
 */
function promiseFiltered(generator, filter =  ((ignore) => true), maxtime = Infinity) {
	var starttime = performance.now();
	maxtime = Number(maxtime);
	return new InspectablePromise(function(resolve, reject, progress) {
		var count = 0;
		var callback = function() {
			var now = performance.now() + 100;
			do {
				var result = generator();
				var found = filter(result);
				++ count;
			} while(!found && now > performance.now());
			progress("Searched through " + count + " results in " + (performance.now() - starttime) + " ms.");
			if(found) {
				resolve(result);
			} else if(Number.isFinite(maxtime) && starttime + maxtime < performance.now()) {
				reject("Timeout after " + (performance.now() - starttime) + "ms");
			} else {
				setTimeout(callback, 0);
			}
		};
		callback();
	});
}
setup.promiseFiltered = promiseFiltered;

/**
 * Generate amount items after filtering, returns the best (largest value) N according to evaluator
 * @param {() => any} generator
 * @param {number=} amount
 * @param {(val: any) => boolean} [filter]
 * @param {(val: any) => number} [evaluator]
 * @param {number} [number]
 */
function bestNFiltered(generator, amount, filter = ((ignore) => true), evaluator = ((val) => val), number = 1) {
	number = Number.isFinite(number) && number > 0 ? number : 1;
	amount = Math.max(Number.isFinite(amount) && amount > 0 ? amount : 100, number);
	var results = [];
	while(results.length < amount) {
		var item = generator();
		if(filter(item)) {
			results.push(item);
		}
	}
	return results.sort((a, b) => Number(evaluator(b)) - Number(evaluator(a))).slice(0, number);
}
setup.bestNFiltered = bestNFiltered;

/**
 * Generate amount items after filtering, returns the best (largest value) according to evaluator
 * @param {() => any} generator
 * @param {number=} amount
 * @param {(val: any) => boolean} [filter]
 * @param {(val: any) => number} [evaluator]
 */
function bestFiltered(generator, amount, filter = ((ignore) => true), evaluator = ((val) => val)) {
	return bestNFiltered(generator, amount, filter, evaluator, 1)[0];
}
setup.bestFiltered = bestFiltered;

/* output formatting */

function round(val, amount) {
	return Math.round(val / amount) * amount;
}

setup.weightInt = function weightInt(weight) {
	var kg = (weight > 20 ? round(weight, 5) : round(weight, 1));
	return (kg + "\u00A0kg");
};

setup.weightUS = function weightUS(weight) {
	var lbs = weight * 2.20462;
	if(lbs > 100) {
			lbs = round(lbs, 10);
	} else if(lbs > 50) {
			lbs = round(lbs, 5);
	} else {
			lbs = round(lbs, 1);
	}
	return (lbs + "\u00A0lbs.");
};

setup.heightInt = function heightInt(height) {
	return (round(height * 100, 1) + "\u00A0cm")
}

setup.heightUS = function heightUS(height) {
	var realFeet = ((height * 39.3700) / 12);
	var feet = Math.floor(realFeet);
	var inches = round((realFeet - feet) * 12, 1);
	return (feet + "\u00A0ft.\u00A0" + inches + "\u00A0in.");
}

/* color manipulation */

setup.color = {
	lch2lab: (col) => {
		const [l, c, h_] = col;
		const h = Number.isNaN(h_) ? 0 : h_ * Math.PI / 180;
		return [l, Math.cos(h) * c, Math.sin(h) * c];
	},
	lab2lch: (col) => {
		const [l, a, b] = col;
		const c = Math.sqrt(a * a + b * b);
		const h = c < 0.00001 ? Number.NaN : (Math.atan2(b, a) * 180 / Math.PI + 360) % 360;
		return [l, c, h];
	},
	lab2xyz: (col) => {
		const [l, a, b] = col;
		const lab_xyz = (c) => c > 6 / 29 ? c * c * c : 108 / 841 * (c - 4 / 29);

		const l_ = (l + 16) / 116;
		var y = lab_xyz(l_);
		var x = lab_xyz(Number.isNaN(a) ? l_ : l_ + a / 500);
		var z = lab_xyz(Number.isNaN(b) ? l_ : l_ - b / 200);
		return [0.950489 * x, 1.000000 * y, 1.088840 * z];
	},
	xyz2lab: (col) => {
		const [x, y, z] = col;
		const xyz_lab = (c) => c > 216 / 24389 ? Math.pow(c, 1/3) : c / 108 * 841 + 4 / 28;

		const y_ = xyz_lab(y / 1.000000);
		var l = 116 * y_ - 16;
		var a = 500 * (xyz_lab(x / 0.950489) - y_);
		var b = 200 * (y_ - xyz_lab(z / 1.088840));
		return [l, a, b];
	},
	xyz2rgb: (col) => {
		const [x, y, z] = col;
		return [3.2404542 * x - 1.5371385 * y - 0.4985314 * z, -0.9692660 * x + 1.8760108 * y + 0.0415560 * z, 0.0556434 * x - 0.2040259 * y + 1.0572252 * z];
	},
	rgb2xyz: (col) => {
		const [r, g, b] = col;
		return [0.4124 * r + 0.3576 * g + 0.1805 * b, 0.2126 * r + 0.7152 * g + 0.0722 * b, 0.0193 * r + 0.1192 * g + 0.9505 * b];
	},
	rgb2srgb: (col) => {
		const [r, g, b] = col;
		const gamma = (c) => 255 * (c <= 0.00304 ? 12.92 * c : 1.055 * Math.pow(c, 1 / 2.4) - 0.055);
		return [gamma(r), gamma(g), gamma(b)];
	},
	srgb2rgb: (col) => {
		const [r, g, b] = col;
		const gamma = (c) => (c <= 10.31475 ? 25 * c / 323 / 255 : Math.pow((c * 200 / 255 + 11) / 211, 2.4));
		return [gamma(r), gamma(g), gamma(b)];
	},
	lab2srgb: (col) => {
		return setup.color.rgb2srgb(setup.color.xyz2rgb(setup.color.lab2xyz(col)));
	},
	srgb2lab: (col) => {
		return setup.color.xyz2lab(setup.color.rgb2xyz(setup.color.srgb2rgb(col)));
	},
	lch2srgb: (col) => {
		return setup.color.lab2srgb(setup.color.lch2lab(col));
	},
	srgb2lch: (col) => {
		return setup.color.lab2lch(setup.color.srgb2lab(col));
	},
	srgb2css: (col) => {
		const [r, g, b] = col;
		const format = (c) => Math.max(0, Math.min(255, Math.round(c))).toString(16).padStart(2, "0");

		return "#" + format(r) + format(g) + format(b);
	},
	css2srgb: (css) => {
		var m = css.match(/^#([0-9a-f]{6})$/i);
		if(m && m[1]) {
			return [parseInt(m[1].substr(0, 2), 16), parseInt(m[1].substr(2, 2), 16), parseInt(m[1].substr(4, 2), 16)];
		}
		return undefined;
	},
	lch2css: (col) => {
		return setup.color.srgb2css(setup.color.lch2srgb(col));
	},
	css2lch: (css) => {
		var srgb = setup.color.css2srgb(css);
		return srgb ? setup.color.srgb2lch(srgb) : undefined;
	},
	lchdisplayable: (col) => {
		var srgb = setup.color.lch2srgb(col);
		return (srgb[0] > -0.5 && srgb[0] < 255.5 && srgb[1] > -0.5 && srgb[1] < 255.5 && srgb[2] > -0.5 && srgb[2] < 255.5);
	}
};

setup.getNPC = function(id) {
    return State.variables[id];
};

setup.getPackage = function(schedules) {
	return schedules.find((schedule) => {
		if(typeof schedule.condition === "function") {
			return schedule.condition();
		} else {
			/* we're at the last, default package */
			return true;
		}
	})
};
