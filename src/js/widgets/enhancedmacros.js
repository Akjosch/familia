/* Added options to add a pop-up, a custom class a custom ID for styling */
Macro.add(['ibutton', 'ilink'], {
	isAsync : true,
	tags    : ["comment"],
	handler() {
		if (this.args.length === 0) {
			return this.error(`no ${this.name === 'ibutton' ? 'button' : 'link'} text specified`);
		}

		const $link = jQuery(document.createElement(this.name === 'ibutton' ? 'button' : 'a'));
		/** @type {string|undefined} */
		let text = undefined;
		/** @type {string|undefined} */
		let passage = undefined;
		/** @type {string[]} */
		let extraClasses = [];
		/** @type {string|undefined} */
		let extraId;
		/** @type {string|undefined} */
		let i18next;

		if (typeof this.args[0] === 'object') {
			if (this.args[0].isImage) {
				// Argument was in wiki image syntax.
				const $image = jQuery(document.createElement('img'))
					.attr('src', this.args[0].source)
					.appendTo($link);

				if (this.args[0].hasOwnProperty('passage')) {
					$image.attr('data-passage', this.args[0].passage);
				}
				if (this.args[0].hasOwnProperty('title')) {
					$image.attr('title', this.args[0].title);
				}
				if (this.args[0].hasOwnProperty('align')) {
					$image.attr('align', this.args[0].align);
				}

				passage = this.args[0].link;
			} else {
				// Argument was in wiki link syntax.
				$link.append(document.createTextNode(this.args[0].text));
				passage = this.args[0].link;
			}
		} else {
			// Argument was simply the link text.
			let passageIdx = 0;
			while(this.args.length > passageIdx) {
				let val = String(this.args[passageIdx]);
				if(val.startsWith(".")) {
					extraClasses.push(val.substring(1));
				} else if(val.startsWith("#")) {
					extraId = val.substring(1);
				} else if(val.startsWith("~")) {
					i18next = val.substring(1);
				} else if(text == null) {
					text = val;
				} else {
					passage = val;
				}
				++ passageIdx;
			}
			text && $link.wikiWithOptions({ profile : 'core' });
		}

		if (passage != null) { // lazy equality for null
			$link.attr('data-passage', passage);

			if (Story.has(passage)) {
				$link.addClass('link-internal');

				if (Config.addVisitedLinkClass && State.hasPlayed(passage)) {
					$link.addClass('link-visited');
				}
			}
			else {
				$link.addClass('link-broken');
			}
		}
		else {
			$link.addClass('link-internal');
		}
		extraClasses.filter((c) => !!c).forEach((c) => $link.addClass(c));
		extraId && $link.attr("id", extraId);
		i18next && $link.attr("data-i18n", i18next);

		$link
			.addClass(`macro-${this.name}`)
			.ariaClick({
				namespace : '.macros',
				one       : passage != null // lazy equality for null
			}, this.createShadowWrapper(
				this.payload[0].contents !== ''
					? () => Wikifier.wikifyEval(this.payload[0].contents.trim())
					: null,
				passage != null // lazy equality for null
					? () => Engine.play(passage)
					: null
			))
			.appendTo(this.output);
	}
});