setup.deathChance = [
	0.320, 0.040, 0.040, 0.030, 0.030, 0.020, 0.020, 0.010, 0.010, 0.010, /*  0 -  9 */
	0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.011, 0.011, 0.011, /* 10 - 19 */
	0.012, 0.012, 0.012, 0.013, 0.013, 0.013, 0.014, 0.014, 0.014, 0.015, /* 20 - 29 */
	0.015, 0.015, 0.016, 0.016, 0.016, 0.017, 0.017, 0.017, 0.018, 0.018, /* 30 - 39 */
	0.018, 0.019, 0.019, 0.019, 0.020, 0.021, 0.022, 0.023, 0.024, 0.025, /* 40 - 49 */
	0.026, 0.027, 0.028, 0.029, 0.030, 0.031, 0.032, 0.033, 0.034, 0.035, /* 50 - 59 */
	0.040, 0.045, 0.050, 0.055, 0.060, 0.070, 0.080, 0.090, 0.100, 0.110, /* 60 - 69 */
	0.120, 0.130, 0.140, 0.150, 0.160, 0.170, 0.180, 0.190, 0.200, 0.220, /* 70 - 79 */
	0.240, 0.260, 0.280, 0.300, 0.320, 0.340, 0.360, 0.380, 0.400, 0.440, /* 80 - 89 */
	0.480, 0.490, 0.500, 0.500, 0.500, 0.500, 0.500, 0.500, 0.500, 0.500  /* 90 - 99 */
];
setup.lifeSpan = [];
var reminder = 1;
var last = 0;
for(let i = 0; i < setup.deathChance.length; ++i) {
	var dead = reminder * setup.deathChance[i];
	last += dead;
	reminder -= dead;
	setup.lifeSpan.push(last);
}
var ages = setup.lifeSpan.reduce((sum, val, i, arr) => sum + (1 - val), 0.7);
var sum = 0.7 / ages;
setup.currentAge = [sum];
for(let i = 0; i < setup.deathChance.length; ++i) {
	var portion = 1 - setup.lifeSpan[i];
	sum += portion / ages;
	setup.currentAge.push(sum);
}

setup.birthChance = [
	0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, /*  0 -  9 */
	0.000, 0.000, 0.000, 0.000, 0.000, 0.030, 0.060, 0.090, 0.130, 0.170, /* 10 - 19 */
	0.230, 0.260, 0.290, 0.320, 0.340, 0.360, 0.350, 0.340, 0.330, 0.310, /* 20 - 29 */
	0.310, 0.300, 0.290, 0.280, 0.260, 0.240, 0.220, 0.200, 0.180, 0.160, /* 30 - 39 */
	0.140, 0.120, 0.100, 0.090, 0.080, 0.070, 0.060, 0.050, 0.040, 0.030, /* 40 - 49 */
	0.020, 0.010, 0.005, 0.004, 0.003, 0.002, 0.002, 0.001, 0.001, 0.001  /* 50 - 59 */
];
var bages = setup.birthChance.reduce((sum, val) => sum + val, 0);
var bsum = 0;
setup.birthYear = [];
for(let i = 0; i < setup.birthChance.length; ++ i) {
	bsum += setup.birthChance[i] / bages;
	setup.birthYear.push(bsum);
}

setup.infertilityChance = [
	0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, /*  0 -  9 */
	0.000, 0.000, 0.000, 0.000, 0.000, 0.010, 0.010, 0.010, 0.010, 0.010, /* 10 - 19 */
	0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, /* 20 - 29 */
	0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, /* 30 - 39 */
	0.020, 0.030, 0.040, 0.050, 0.060, 0.070, 0.080, 0.100, 0.120, 0.140, /* 40 - 49 */
	0.170, 0.200, 0.250, 0.300, 0.400, 0.500, 0.600, 0.700, 0.800, 0.900  /* 50 - 59 */
];
var ireminder = 1;
var isum = 0;
setup.infertilityYear = [];
for(let i = 0; i < setup.infertilityChance.length; ++ i) {
	var infertile = ireminder * setup.infertilityChance[i];
	isum += infertile;
	ireminder -= infertile;
	setup.infertilityYear.push(isum);
}

setup.tableRnd = function(table, rnd) {
	var val = (rnd || Math.random)();
	return table.findIndex((v) => v > val);
}

setup.rollD6 = function(rnd) {
	rnd = rnd || Math.random;
	return Math.floor(rnd() * 6) + 1;
}

setup.rollD6ex = function(rnd) {
	rnd = rnd || Math.random;
	var result = 0;
	do {
		var roll = setup.rollD6(rnd);
		result += roll;
	} while(roll === 6);
	return result;
}