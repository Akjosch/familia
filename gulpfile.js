const { src, dest, series, task } = require('gulp');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const Stream = require('stream');
const { spawnSync  } = require('child_process');
const Path = require('path');
const del = require('del');
const os = require('os');

const target = {
	file: "bin/game.html",
	ext: "bin/ext/"
};
const source = {
	twine: "src/twine",
	build: "build",
	ext: "ext",
	js: "src/js",
	i18n: "src/i18n",
	modules: "src/modules"
};

var tool = "tools/tweego";
switch(os.platform()) {
	case "win32":
		tool = tool + "_win.exe";
		break;
	case "linux":
		tool = tool + "_nix";
		break;
	case "darwin":
		tool = tool + "_osx";
		break;
	default:
		throw new Error("Unsupported OS " + os.type() + " " + os.release() + " (" + os.platform() + ")");
}
console.log(os.type() + " " + os.release() + " (" + os.platform() + ")");

task("clean", (done) => {
	del.sync(source.build + "/**");
	done();
});

task("compile", (done) => {
	del.sync(target.file);
	let error = spawnSync(tool,  ['--head=header.txt', '-m', source.modules, '-o', target.file, source.twine, source.build]).stderr;
	if(error.length) {
		throw new Error(String(error));
	}
	done();
})

task("copyext", (done) => {
	del.sync(target.ext + "/**");
	src(source.ext + "/**").pipe(dest(target.ext + "/"));
	done();
})

task("createlang", () => {
	var stream = new Stream.Transform({ objectMode: true });
	stream._transform = function(file, unused, callback) {
		const extname = Path.extname(file.relative);
		const basename = Path.basename(file.relative, extname);
		const header = ":: " + basename + " [i18next]\n";
		file.contents = Buffer.concat([Buffer.from(header), file.contents]);
		callback(null, file);
	};

	return src(source.i18n + "/*.json").pipe(stream).pipe(rename({ extname: '.tw' })).pipe(dest(source.build + "/i18n/"));
});

task("copyjs", () => {
	return src(source.js + "/**/*.js")
		.pipe(dest(source.build + "/js"));
});

task("babelize", () => {
	return src(source.js + "/**/*.js")
		.pipe(babel({
            presets: ['@babel/env']
        }))
		.pipe(dest(source.build + "/js"));
});

task("build", series("createlang", "babelize", "compile", "copyext"));
task("quickbuild", series("createlang", "copyjs", "compile", "copyext"));
task("default", series("clean", "build"));
